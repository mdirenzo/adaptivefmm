#ifndef chebyshev_h
#define chebyshev_h
#include <math.h>

class chebyshev
{
 public:
  float PI;
  int n; //number of chebyshev nodes
  
  float3* nodes;
  float** coeff;
  
  void set_chebyshev_polynomials();
  float T(int,float);
  float S(int,float,float);
  float R(int,float3,float3);
  //overload R, to support float4 (but still .x .y .z fields are useful)
  float R(int,float4,float4);
  float R(int,float4,float3);
  float R(int,float3,float4);
  void set_chebyshev_nodes();
  float4 scale(float4,float3,float);
  float3 scale2(float3,float3, float);
  float3 scale(float3,float3,float3);

  chebyshev(){}
  ~chebyshev(){}
  chebyshev(int);  
};
  
#endif



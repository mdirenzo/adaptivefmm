OBJS = fmm.o node.o position.o triple.o routines.o chebyshev.o precomputation.o kernel.o
CPP= g++
NVIDIA= nvcc
CFLAGS= -O3
NFLAGS= -O3 -arch=sm_20

default: fmm gen_uniform gen_spiral gen_uniform gen_spherical

fmm: $(OBJS)
	$(NVIDIA) $(NFLAGS) $(OBJS) -o fmm

fmm.o: fmm.cu node.h node.cu triple.cu triple.h position.h position.cu routines.h routines.cu chebyshev.h chebyshev.cu cpu_kernels.h
	$(NVIDIA) $(NFLAGS) -c fmm.cu

routines.o: routines.h routines.cu
	$(NVIDIA) $(NFLAGS) -c routines.cu

kernel.o: kernel.h kernel.cu
	$(NVIDIA) $(NFLAGS) -c kernel.cu

precomputation.o: precomputation.h precomputation.cu
	$(NVIDIA) $(NFLAGS) -c precomputation.cu

chebyshev.o: chebyshev.h chebyshev.cu
	$(NVIDIA) $(NFLAGS) -c chebyshev.cu

node.o: node.cu node.h triple.cu triple.h position.cu triple.h
	$(NVIDIA) $(NFLAGS) -c node.cu	

triple.o: node.cu node.h triple.cu triple.h position.cu position.h
	$(NVIDIA) $(NFLAGS) -c triple.cu	

position.o: position.cu triple.h
	$(NVIDIA) $(NFLAGS) -c position.cu

gen_spiral: gen_spiral.cpp
	$(CPP) -o gen_spiral gen_spiral.cpp $(CFLAGS)

gen_uniform: gen_uniform.cpp
	$(CPP) -o gen_uniform gen_uniform.cpp $(CFLAGS)

gen_spherical: gen_spherical.cpp
	$(CPP) -o gen_spherical gen_spherical.cpp $(CFLAGS)

clean:
	rm -rf fmm gen_spiral gen_uniform gen_spherical *.o

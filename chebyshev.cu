#include "chebyshev.h"

/*
All funcion notations are consistent with the following paper:
"The black-box fast multipole method" by William Fong and Eric Darve
 */
chebyshev::chebyshev(int nn)
{
  n = nn;
  PI = 3.14159265359;
  nodes = new float3[n*n*n];
  coeff = new float*[n+1];
  for ( int i(0); i < n+1; i++ )
    {
      coeff[i] = new float[n+1];
    }
} 

void chebyshev::set_chebyshev_polynomials()
{
  for (int i(0); i < n+1; i++)
    {
      for ( int j(0); j < n+1; j++)
	coeff[i][j] = 0;
    }
  coeff[0][0] = 1;
  coeff[1][1] = 1;
  for ( int i(2); i < n+1; i++ )
    {
      coeff[i][0] =- coeff[i-2][0];
      for ( int j(1); j < n+1; j++)
	coeff[i][j] = 2 * coeff[i-1][j-1] - coeff[i-2][j];
    }
}
float chebyshev::T( int m, float x )
{
  float ans = coeff[m][0];
  float y = x;
  for ( int j(1); j < m+1; j++ )
    {
      ans += coeff[m][j] * y;
      y *= x;
    }
  return ans;
}


float chebyshev::S( int m, float x, float y )
{
  float ans(0);
  for ( int k(1); k < m; k++ )
    ans += T(k,x) * T(k,y);
  ans *= 2. / m;
  ans += 1. / m;
  return ans;
}

float chebyshev::R( int m, float3 X, float3 Y )
{
  return S( m, X.x, Y.x ) * S( m, X.y, Y.y ) * S( m, X.z, Y.z );
}

//overload R, to support float4 (but still .x .y .z fields are useful)
float chebyshev::R( int m, float4 X, float4 Y )
{
  return S( m, X.x, Y.x ) * S( m, X.y, Y.y ) * S( m, X.z, Y.z );
}

float chebyshev::R( int m, float4 X, float3 Y )
{
  return S( m, X.x, Y.x ) * S( m, X.y, Y.y ) * S( m, X.z, Y.z );
}

float chebyshev::R( int m, float3 X, float4 Y )
{
  return S( m, X.x, Y.x ) * S( m, X.y, Y.y ) * S( m, X.z, Y.z );
}

void chebyshev::set_chebyshev_nodes()
{
  for ( int i = 1; i <= n; i++ )
    {
      for ( int j = 1; j <= n; j++ )
	{
	  for ( int k = 1; k <= n; k++ )
	    {
	      nodes[ ( n*(i-1) + (j-1) ) * n + (k-1) ].x = cos( (2*i - 1) * PI / (2*n) );
	      nodes[ ( n*(i-1) + (j-1) ) * n + (k-1) ].y = cos( (2*j - 1) * PI / (2*n) );
	      nodes[ ( n*(i-1) + (j-1) ) * n + (k-1) ].z = cos( (2*k - 1) * PI / (2*n) );
	    }
	}
    }
}

//ans = (A-S)*r
float4 chebyshev::scale( float4 A, float3 S, float r )
{
  A.x -= S.x;
  A.y -= S.y;
  A.z -= S.z;
  A.x *= r;
  A.y *= r;
  A.z *= r;
  return A;
}

//ans = (A/r)+S
float3 chebyshev::scale2( float3 A, float3 S, float r )
{
  A.x /= r;
  A.y /= r;
  A.z /= r;
  A.x += S.x;
  A.y += S.y;
  A.z += S.z;
  return A;
}

/* 
define V= vector connectng S1 to S2 Multiplied by 2 = 2*(S2-S1);
then ans = (A-V)/2 = A/2 - (S2-S1)
designed specifically for M2M: so S1 is center of child and s2 is center of box of interest
*/
float3 chebyshev::scale( float3 A, float3 S1, float3 S2 )
{
  A.x /= 2;
  A.y /= 2;
  A.z /= 2;
  A.x -= ( S2.x - S1.x );
  A.y -= ( S2.y - S1.y );
  A.z -= ( S2.z - S1.z );
  return A;
}

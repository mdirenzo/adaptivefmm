#ifndef triple_h
#define triple_h

#include "routines.h"

class node;

class triple
{
 public:
  int x;
  int y;
  int z;
  node *pointer;
  triple();
  triple(int,int,int);
  triple(int);
  ~triple(){}
  triple operator+(triple);
  triple operator-(triple);
  triple operator=(triple);
  triple operator*(int);
  void set(int a,int b,int c){x=a;y=b;z=c;}
  int b2i(); //biinary to integer
  void i2b(int); //integer to binary
  int t2i(); //ternary to integer
  int h2i(); //hex to integer
  void i2h(int); //integer to hex
  int norm(); // return infinity-norm of a triple vector
};

#endif

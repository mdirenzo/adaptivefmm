#ifndef routines_h
#define routines_h
#include<fstream>
#include<string>
#include<sstream>
#include<iostream>
#include<time.h>

extern int Nx,Ny,Nz;

double diffclock(clock_t,clock_t);
float3 minimum(float3*,int); //get array of float3 and return minimum in each direction
float abs1(float x);
//overloaded for abs
float4 minimum(float4*,int); //get array of float3 and return minimum in each direction
float3 maximum(float3*,int); //get array of float3 and return maximum in each direction
int maximum(int,int,int);
void new_grid(float***&,int,int,int);
void new_grid(float****&,int,int,int,int);
void delete_grid(float***&,int,int,int);
void zero(float ***,int,int,int);
void tecplot(float ***,int,int,int);
//true if points A and B are almost equal
bool equal(float3,float3);
#endif

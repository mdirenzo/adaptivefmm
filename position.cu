#include "position.h"

position::position( float3 A, float3 B )
{
  set_diag( A, B );
}

void position::set_all()
{
  corners_[1].z = corners_[7].z; 
  corners_[1].x = corners_[0].x; 
  corners_[1].y = corners_[0].y;
  
  corners_[2].y = corners_[7].y; 
  corners_[2].x = corners_[0].x; 
  corners_[2].z = corners_[0].z;
  
  corners_[3].x = corners_[0].x;
  corners_[3].y = corners_[7].y;
  corners_[3].z = corners_[7].z;
  
  corners_[4].x = corners_[7].x; 
  corners_[4].y = corners_[0].y;
  corners_[4].z = corners_[0].z;
  
  corners_[5].y = corners_[0].y; 
  corners_[5].x = corners_[7].x;
  corners_[5].z = corners_[7].z;
  
  corners_[6].z = corners_[0].z;
  corners_[6].x = corners_[7].x; 
  corners_[6].y = corners_[7].y;
  
  position::CENTER_.x = .5 * ( corners_[0].x + corners_[7].x );
  position::CENTER_.y = .5 * ( corners_[0].y + corners_[7].y );
  position::CENTER_.z = .5 * ( corners_[0].z + corners_[7].z );
}

void position::set_diag( float3 A, float3 B )
{
    corners_[0] = A;
    corners_[7] = B;
}

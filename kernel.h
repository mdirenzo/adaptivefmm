#ifndef kernel_h
#define kernel_h

class kernel
{
 public:
  int lambda;
  float k(float3 X,float3 Y);
  float k(float4 X,float3 Y);
  float k(float3 X,float4 Y);
  float k(float4 X,float4 Y);
  kernel(){lambda=-1;}
  ~kernel(){}
};


#endif

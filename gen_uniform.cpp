#include<iostream>
#include<fstream>
#include <stdio.h>
#include <stdlib.h>
#include<math.h>

using namespace std;
int main(int argc,char * const argv[])
{
  ofstream output(argv[2]);
  if (argc!=3)
    {
      cout<<"please enter number of points and output file address (e.g. 1000 data.dat)"<<endl;
      exit(1);
    }
  int N=atoi(argv[1]);
  float x,y,z;
  int root3=(int)(pow(N+1,1./3.));
  cout<<"Actual number of points = "<<root3<<"^3 = "<<root3*root3*root3<<endl;
  output<<root3*root3*root3<<endl;
  for (int i(0);i<root3;i++)
    for (int j(0);j<root3;j++)
      for (int k(0);k<root3;k++)
	{
	  x=float(i)/float(root3)*2-1;
	  y=float(j)/float(root3)*2-1;
	  z=float(k)/float(root3)*2-1;
	  output<<x<<" "<<y<<" "<<z<<" "<<1<<endl;
	}
  output.close();
}

#include "precomputation.h"
#include "chebyshev.h"
#include "node.h"
#include "kernel.h"
#include "position.h"
#include "triple.h"
#include "routines.h"

precomputation::precomputation( chebyshev *cheby )
{
  cheb = cheby;
  n = cheb->n;

  new_grid( matrix_M2M, 8, n*n*n, n*n*n );
  new_grid( matrix_M2L, 8, 216, n*n*n, n*n*n);
  precompute();
}
  
float3 precomputation::midpoint( float3 a, float3 b )
{
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  a.x /= 2.;
  a.y /= 2.;
  a.z /= 2.;
  return a;		
}

void precomputation::precompute()
{    
  kernel kern;
  //M2M claculation
  float3 A;  
  A.x = -1;  
  A.y = -1;  
  A.z = -1;
  
  float3 B; 
  B.x = 1;
  B.y = 1; 
  B.z = 1;
  
  position parent( A, B );
  parent.set_all();
  A = parent.center();
  
  for ( int i(0); i < 8; i++ )
    {
      B = midpoint( A, parent.corners()[i] );
    for (int l(0);l<n*n*n;l++)
      {
	for ( int m(0) ; m < n*n*n; m++ )
	  {
	    matrix_M2M[i][l][m] = cheb->R( n, cheb->nodes[l], cheb->scale( cheb->nodes[m], B, A ) );
	  }
      }
    }
  
  //L2L
  matrix_L2L = matrix_M2M;
  
  //M2L
  triple which;
  triple interactant;
  A.x = 0;
  A.y = 0;
  A.z = 0;

  B = A;
  
  for ( int i(0); i < 8; i++)
    {
      which.i2b(i);
      for ( int j(0); j < 216; j++ )
	{
	  interactant.i2h(j);
	  interactant = interactant - which;
	  if ( interactant.norm() > 1 )
	    {
	      A.x = interactant.x * 2;
	      A.y = interactant.y * 2;
	      A.z = interactant.z * 2;
	      for ( int m(0); m < n*n*n; m++ )
		{
		  for (int l(0); l < n*n*n; l++ )
		    {
		      matrix_M2L[i][j][m][l] = kern.k( cheb->scale2( cheb->nodes[l], B, 1 ), cheb->scale2( cheb->nodes[m], A, 1 ) );
		    }
		}
	    }
	}
    }
}

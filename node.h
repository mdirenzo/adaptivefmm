#ifndef node_h
#define node_h
/*
This is the node class in order to define the FMM tree.
 */
class chebyshev;
class precomputation;
class kernel;

#include "position.h"
#include "triple.h"
#include <vector>

class node
{
  static const float epsilon = 1e-6; //this is the epsilon of this node (for equality and inequality checking)
  bool isleaf_; //true if this node is a leaf of tree
  int threshold_; //maximum number of points per leaf
  int lmax_; //maximum allowed level of the tree
  node *children_[8];
  node *parent_; //pointer to parent
  int which_; //shows this node is which child of its parent (e.g. =6)
  triple which_binary_; //the same as which_ but in bnary
  int num_; //number of points in this box
  float4 *data_; //pointer to the first element of array of points (x,y,z,density)
  float *f_; //potential at target points
  float *M_; //Multipoles, M-list
  float *L_; //Locals, L-list
  int level_; //level(depth) of this node in tree
  int max_depth_; //distance to the farthest leave in corresponding subtree.
  int sortbyx(int,int); //sort by x-component of data  ,  output: position of first element in array which data>average ,input arguments:start point and end point in the array of data (e.g. start=0, end=n)
  int sortbyy(int,int);
  int sortbyz(int,int);
  void divide(); //divide data between childrens 
  bool is_inside(float3); //return true if given point is inside this node
  bool is_adjacent(node *); //true if given node is adjacent to this node
  std::vector<node*> list_L_; //List of all leaf octants in a subtree that this node is its root
  std::vector<node*> list_A_; //List of ancestors of this node
  std::vector<node*> list_D_; //List of descendants of this node
  std::vector<node*> list_C_; //List of colleagues of this node
  std::vector<node*> list_U_; //List of adjacent leaves of this node (if it is e leaf it self)
  std::vector<node*> list_X_; //X List
  std::vector<node*> list_W_; //W List
  std::vector<node*> list_V_; //V List
  triple colleagues_[27];
  triple interactants_[216];
 public:
  int n;
  position coordinate; //physical coordinates of box
  chebyshev* cheb;
  precomputation* pcomp;
  kernel* kern;
  node(int,int,int,float4*,float3,float3,node*,chebyshev*,precomputation*,kernel*); //(threshold_particle,lmax, number of data in this node,pointer to first data on memory,coordinate of LBR,coordinate of RTF, pointer to parent)
  ~node();
  std::vector<node*>* list_L(){return &list_L_;} //return pointer to L list
  std::vector<node*>* list_A(){return &list_A_;} //return pointer to A list
  std::vector<node*>* list_D(){return &list_D_;} //return pointer to D list
  std::vector<node*>* list_C(){return &list_C_;} //return pointer to C list
  std::vector<node*>* list_U(){return &list_U_;} //return pointer to U list
  std::vector<node*>* list_X(){return &list_X_;} //return pointer to X list
  std::vector<node*>* list_W(){return &list_W_;} //return pointer to W list
  std::vector<node*>* list_V(){return &list_V_;} //return pointer to V list
  void make_list_C_V(); //make list C (should be used by proper algorithm)
  void make_list_U_X_W(); //make list C (should be used by proper algorithm)
  node** children(){return children_;}
  triple* colleagues(){return colleagues_;}
  triple* interactants(){return interactants_;}
  node* parent(){return parent_;}
  int level(){return level_;}
  int max_depth(){return max_depth_;}
  int which(){return which_;}
  triple which_binary(){return which_binary_;}
  void parent(node *A) {parent_=A;}
  void level(int a){level_=a;}
   void which(int a){which_=a;}
  void which_binary(triple a){which_binary_=a;}
  void which_binary(int a){which_binary_.i2b(a);}
  float* f(){return f_;}
  float* M(){return M_;}
  float* L(){return L_;}
  bool isleaf(){return isleaf_;}
  int num(){return num_;}
  float4* data(){return data_;}
  
}; 

/* Related algorithm functions */
void make_list_C_V(node*);
void make_list_U_X_W(node*);
#endif

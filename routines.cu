#include "routines.h"

double diffclock( clock_t clock1, clock_t clock2 )
{
  double diffticks = clock1 - clock2;
  double diffms = ( diffticks * 1000 ) / CLOCKS_PER_SEC;
  return diffms;
}


float3 minimum( float3*a, int n ) //get array of float3 and return minimum in each direction
{
  float3 m = a[0];
  for ( int i(1); i < n; i++ )
     {
       if ( a[i].x < m.x )
	 {
	   m.x = a[i].x;
	 }
       if ( a[i].y < m.y )
	 {
	   m.y = a[i].y;
	 }
       if ( a[i].z < m.z)
	 {
	   m.z = a[i].z;
	 }
     }
  return m; 
}

float abs1( float x )
{
  if ( x==0 )
    {
      return 1;
    }
  return ( x>0 ) ? x : -x;
}

//overloaded for abs
float4 minimum( float4*a, int n ) //get array of float3 and return minimum in each direction
{
  float4 m;
  m.x = m.y = m.z = 0;
  for ( int i(0); i < n; i++ )
    {
      if ( abs1( a[i].x ) < abs1( m.x ) ) 
	{
	  m.x = a[i].x;
	}
      if ( abs1( a[i].y ) < abs1( m.y ) ) 
	{
	  m.y = a[i].y;
	}
      if ( abs1( a[i].z ) < abs1( m.z ) ) 
	{
	  m.z = a[i].z;
	}
    }
  return m; 
}

float3 maximum( float3*a, int n ) //get array of float3 and return maximum in each direction
{
  float3 m = a[0];
  for ( int i(1); i < n; i++ )
    {
      if ( a[i].x > m.x )
	{
	  m.x = a[i].x;
	}
      if ( a[i].y > m.y)
	{
	  m.y = a[i].y;
	}
      if ( a[i].z > m.z)
	{
	  m.z = a[i].z;
	}
    }
  return m; 
}

int maximum( int x, int y, int z )
{
  x = ( x > y ) ? x : y;
  x = ( x > z ) ? x : z;
  return x;
}

void new_grid( float*** &a, int Nx, int Ny, int Nz )
{
  a = new float**[Nx];
  for ( int i(0); i < Nx; i++ )
    {
      a[i] = new float*[Ny];
      for ( int j(0); j< Ny; j++ )
	{
	  a[i][j] = new float[Nz];
	}
    }
}

void new_grid( float**** &a, int Nx, int Ny, int Nz, int Nw )
{
  a = new float***[Nx];
  for ( int i(0); i < Nx; i++ )
    {
      a[i] = new float**[Ny];
      for ( int j(0); j < Ny; j++ )
	{
	  a[i][j] = new float*[Nz];
	  for ( int k(0); k < Nz; k++ )
	    a[i][j][k] = new float[Nw];
	}
    }
}

void delete_grid( float*** &a, int Nx, int Ny, int Nz )
{
  for ( int i(0); i < Nx; i++ )
    {
      for ( int j(0); j < Ny; j++ )
	delete [] a[i][j];
      delete a[i];
    }
  delete [] a;
}


void zero( float ***A, int Nx, int Ny, int Nz )
{
  for ( int i(0); i < Nx; i++ )
    {
    for ( int j(0); j < Ny; j++ )
      {
	for ( int k(0); k < Nz; k++ )
	  {
	    A[i][j][k] = 0;
	  }
      }
    }
}


// true if points A and B are almost equal
bool equal( float3 A, float3 B )
{
  return ( ( (A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y)  +(A.z - B.z) * (A.z - B.z) ) <= 0.0000001 );
}

#ifndef position_h
#define position_h

class position
{
  float3 corners_[8];
  float3 CENTER_; //Center of box
 public:
  position(){}
  position(float3,float3); //do the same as set, but in construction
  ~position(){}
  void set_diag(float3,float3); //takes position of two corners on the diagonal (minX,minY,minZ) (maxX,maxY,maxZ)
  void set_all(); //compute position of all other corners/center based on LBR and RTF
  float3 center(){return CENTER_;}
  float3* corners(){return corners_;}
};

#endif

#include "triple.h"

class node;

triple::triple()
{
  x = 0;
  y = 0;
  z = 0;
  pointer = NULL;
}

triple::triple( int a, int b, int c )
{
  x = a;
  y = b;
  z = c;
  pointer = NULL;
}

triple::triple( int a )
{
  z = a % 2;
  a /= 2;
  y = a % 2;
  a /= 2;
  x = a % 2;
  pointer = NULL;
}

triple triple::operator+( triple B )
{
  triple C( x + B.x, y + B.y, z + B.z );
  return C;
}

triple triple::operator-( triple B )
{
  triple C( x - B.x, y - B.y, z - B.z );
  return C;
}

triple triple::operator=( triple B )
{
  x = B.x; 
  y = B.y; 
  z = B.z; 
  pointer = B.pointer;
  return B;
}

triple triple::operator*( int a )
{
  triple C;
  C.x = x * a;
  C.y = y * a;
  C.z = z * a;
  C.pointer = pointer;
  return C;
}

int triple::b2i()
{
  return x*4 + y*2 + z;
}

void triple::i2b( int a )
{
  z = a % 2;
  a /= 2;
  y = a % 2;
  a /= 2;
  x = a % 2;
}

int triple::t2i() //when x,y, and z are -1,0,+1 
{
  return (x+1)*9 + (y+1)*3 + (z+1);
}

int triple::h2i() //when x,y, and z are -2,-1,0,1,2,3
{
  return (x+2)*36 + (y+2)*6 + (z+2);
}

void triple::i2h(int a)
{
  z = (a % 6) - 2;
  a /= 6;
  y = (a % 6) - 2;
  a /= 6;
  x = (a % 6) - 2;
}

int triple::norm()
{
  return maximum( abs(x), abs(y), abs(z) );
}
